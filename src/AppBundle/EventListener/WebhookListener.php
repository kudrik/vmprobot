<?php

namespace AppBundle\EventListener;

use kudrmudr\SnDataProviderBundle\Event\MessageEvent;
use kudrmudr\SnDataProviderBundle\Entity\Message;
use GuzzleHttp\Client;
use kudrmudr\SnDataProviderBundle\Service\Driver;

class WebhookListener
{
    protected $driver;
    protected $client;

    public function __construct(Driver $driver, Client $client)
    {
        $this->driver = $driver;
        $this->client = $client;
    }

    public function message(MessageEvent $event)
    {
        $message = $event->getMessage();

        $answer = new Message();
        $answer->setUser($message->getUser());
        $answer->setType(Message::MSG_TYPE_PM);

        try {

            $vmpro_url = 'https://api.video-cdn.net/v1/vms';

            $fields = [
                'username' => 'constantin.plotkin@movingimage.com',
                'password' => 'Plotkin54',
            ];

            $res = $this->client->post($vmpro_url.'/auth/login', [\GuzzleHttp\RequestOptions::JSON => $fields]);

            $querys = [
                //'include_custom_metadata' => 'true',
                'search_in_field' => 'title',
                'search_term' => $message->getText(),
            ];

            $headers = [
                'Authorization' => 'Bearer '.json_decode($res->getBody()->getContents())->accessToken
            ];

            $res = $this->client->get($vmpro_url.'/710/videos', [\GuzzleHttp\RequestOptions::HEADERS => $headers, \GuzzleHttp\RequestOptions::QUERY => $querys]);

            $video = array_shift(\json_decode($res->getBody()->getContents())->videos);

            if ($video->id) {
                $answer->setText('https://e.video-cdn.net/video?video-id='.$video->id.'&player-id=2cmTmN9QAoxVXdzXEHmBEW');
            }

        } catch (\Exception $e) {


        }

        if (!$answer->getText()) {
            $answer->setText('not found');
        }

        $this->driver->send($answer);
    }
}